import javax.swing.*;

/**
 * @author iusenko
 */
public class GameManager {

    private static final int TIMER_TIC_MS = 500;

    interface TimerDelegate {
        void onTimer(int total, int passed);
    }

    private final TimerDelegate delegate;
    private Timer timer;

    public GameManager(TimerDelegate delegate) {
        this.delegate = delegate;
    }

    public void start() {
        if (timer != null) {
            throw new IllegalStateException();
        }
        timer = new Timer(100, new Main.IncAction());
    }

    public void stop() {
        if (timer == null) return;
        timer.stop();
        timer = null;
    }
}

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author zhenya
 */
public class Main extends JFrame {

    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 380;

    private List<JLabel> buttons;
    private JProgressBar progressBar;
    private JButton startButton;
    private JButton stopButton;
    private float counter = 0;
    private Timer gameTimer = new Timer(100, new IncAction());
    private Color progressColor;
    private int lastPresedNumeber = 0;

    public Main() {
        setTitle("5x5");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new BorderLayout());
        initCenterPanel();
        initBottomPanel();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    private void initButtons() {
        buttons = new ArrayList<JLabel>(25);
        for (int i = 0; i < 25; i++) {
            JLabel l = new JLabel("" + i);
            l.setHorizontalAlignment((int) JLabel.CENTER_ALIGNMENT);
            l.setVerticalAlignment((int) JLabel.CENTER_ALIGNMENT);
            l.addMouseListener(new PressButtonListener());
            l.setBorder(BorderFactory.createEtchedBorder());
            buttons.add(l);
        }
    }

    private void initCenterPanel() {
        JPanel p = new JPanel(new GridBagLayout());
        p.setBorder(BorderFactory.createEtchedBorder());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        initButtons();
        int buttonnum = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++, buttonnum++) {
                c.gridx = j;
                c.gridy = i;
                p.add(buttons.get(buttonnum), c);
            }
        }
        getContentPane().add(BorderLayout.CENTER, p);
    }

    private void initBottomPanel() {
        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = c.gridy = 0;
        c.insets = new Insets(10, 10, 10, 5);
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        progressBar = new JProgressBar(0, 25);
        progressBar.setStringPainted(true);
        progressColor = progressBar.getForeground();
        progressBar.setString("0");
        p.add(progressBar, c);

        c.gridx++;
        c.weightx = 0.0;
        c.insets = new Insets(10, 5, 10, 5);
        stopButton = new JButton("Стоп");
        stopButton.addActionListener(new StopGameAction());
        p.add(stopButton, c);

        c.gridx++;
        c.insets = new Insets(10, 10, 10, 10);
        startButton = new JButton("Старт");
        startButton.addActionListener(new StarGameAction());
        p.add(startButton, c);

        getContentPane().add(BorderLayout.SOUTH, p);
    }


    /**
     * Метод перетасовует иконки игрового поля.
     * Алгоритм взят из http://www.simplecoding.org/tshhatelnaya-peretasovka-kolody-kart.html
     *
     * @param labels
     */
    @SuppressWarnings("static-access")
    private void mix(List<JLabel> labels) {
        //создаем генератор случайных чисел, в качестве начального
        //значения передаем системное время
        Random generator = new Random(new Date().getTime());
        for (int i = 0; i < labels.size(); i++) {
            //генерируем случайное число, в диапазоне от нуля до конца колоды
            int newPos = generator.nextInt(labels.size());
            //меняем местами текущую карту с картой, которая находится в labels[i]
            int curCard = Integer.parseInt(labels.get(i).getText());
            labels.get(i).setText(labels.get(newPos).getText());
            labels.get(newPos).setText("" + curCard);

            //для увеличения эффекта "случайности" возникновения чисел, в течении перетасовки колоды,
            // четыре раза устанавливаем новое начальное значение генератора случайных чисел
            if (i % (labels.size() / 4) == 0) {
                int pause = generator.nextInt(20); //генерируем случайный интервал времени (мс)
                try {
                    //останавливаем работу программы на полученный интервал времени (максимально возможная
                    // задержка восемдесят миллисекунд)
                    Thread.currentThread().sleep(pause);
                } catch (InterruptedException ex) {
                }
                //уставливаем новое начальное значение генератора
                generator.setSeed(new Date().getTime());
            }
        }
    }

    public static void main(String[] args) {
        (new Main()).setVisible(true);
    }

    private class PressButtonListener extends MouseAdapter {
        @Override
        public void mouseReleased(MouseEvent e) {

            int presed = Integer.parseInt(((JLabel) e.getSource()).getText());
            if (lastPresedNumeber == presed) {
                for (JLabel l : buttons) l.setForeground(Color.BLACK);
                ((JLabel) e.getSource()).setForeground(Color.LIGHT_GRAY);
                lastPresedNumeber++;
            }
            if (lastPresedNumeber == 25) {
                progressBar.setString(String.format("%.1f", counter) + " Завершено!");
                gameTimer.stop();
            }
        }
    }

    private class IncAction extends AbstractAction {
        public void actionPerformed(ActionEvent e) {
            counter += 0.1;
            progressBar.setString(String.format("%.1f", counter));
            progressBar.setValue((int) counter);
            if (counter > 25) {
                progressBar.setForeground(Color.RED);
            } else {
                progressBar.setForeground(progressColor);
            }

        }
    }

    private class StarGameAction extends AbstractAction {
        public void actionPerformed(ActionEvent e) {
            counter = 0;
            lastPresedNumeber = 0;
            progressBar.setString("0");
            gameTimer.start();
            mix(buttons);
            for (JLabel l : buttons) l.setForeground(Color.BLACK);
        }
    }

    private class StopGameAction extends AbstractAction {
        public void actionPerformed(ActionEvent e) {
            gameTimer.stop();
            for (JLabel l : buttons) l.setForeground(Color.BLACK);
        }
    }
}
